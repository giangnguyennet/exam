# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

100.times.each do |i|
  index = '%03i' % i
  User.where(email: "example_#{index}@example.com").first_or_create
end

Game.where(name: "The Legend of Zelda: Ocarina of Time").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Chrono Trigger").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Final Fantasy VII").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Super Mario 64").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Street Fighter II").first_or_create.update price: rand(1..10) * 100
Game.where(name: "The Legend of Zelda: A Link to the Past").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Super Mario World").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Metal Gear Solid").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Super Metroid").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Resident Evil 4").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Super Mario Kart").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Castlevania: Symphony of the Night").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Super Mario Bros.").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Super Mario Bros. 3").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Tetris").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Half-Life 2").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Final Fantasy VI").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Resident Evil").first_or_create.update price: rand(1..10) * 100
Game.where(name: "GoldenEye 007").first_or_create.update price: rand(1..10) * 100
Game.where(name: "SoulCalibur").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Shadow of the Colossus").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Pac-Man").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Pokémon Red & Blue").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Donkey Kong").first_or_create.update price: rand(1..10) * 100
Game.where(name: "The Legend of Zelda").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Sonic the Hedgehog").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Space Invaders").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Punch-Out!!").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Contra").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Doom").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Half-Life").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Halo: Combat Evolved").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Metal Gear Solid 2: Sons of Liberty").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Super Smash Bros. Melee").first_or_create.update price: rand(1..10) * 100
Game.where(name: "The Legend of Zelda: The Wind Waker").first_or_create.update price: rand(1..10) * 100
Game.where(name: "World of Warcraft").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Super Mario Galaxy").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Secret of Mana").first_or_create.update price: rand(1..10) * 100
Game.where(name: "The Legend of Zelda: Majora's Mask").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Tomb Raider").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Resident Evil 2").first_or_create.update price: rand(1..10) * 100
Game.where(name: "StarCraft").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Final Fantasy X").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Metroid Prime").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Prince of Persia: The Sands of Time").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Call of Duty 4: Modern Warfare").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Ms. Pac-Man").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Double Dragon").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Super Mario World 2: Yoshi's Island").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Final Fantasy Tactics").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Grand Theft Auto III").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Ico").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Silent Hill 2").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Star Wars: Knights of the Old Republic").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Metal Gear Solid 3: Snake Eater").first_or_create.update price: rand(1..10) * 100
Game.where(name: "BioShock").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Galaga").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Sonic the Hedgehog 2").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Shenmue").first_or_create.update price: rand(1..10) * 100
Game.where(name: "The Last of Us").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Mega Man 2").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Final Fantasy IV").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Daytona USA").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Advance Wars").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Metroid").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Gunstar Heroes").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Xenogears").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Deus Ex").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Kingdom Hearts").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Grand Theft Auto: San Andreas").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Grand Theft Auto IV").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Uncharted 2: Among Thieves").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Mass Effect 2").first_or_create.update price: rand(1..10) * 100
Game.where(name: "OutRun").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Grim Fandango").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Portal").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Red Dead Redemption").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Super Mario Galaxy 2").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Dark Souls").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Grand Theft Auto V").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Portal 2").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Virtua Fighter 2").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Nights into Dreams...").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Gran Turismo").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Panzer Dragoon Saga").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Counter-Strike").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Ōkami").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Fallout 3").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Metal Gear Solid 4: Guns of the Patriots").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Street Fighter IV").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Batman: Arkham City").first_or_create.update price: rand(1..10) * 100
Game.where(name: "The Elder Scrolls V: Skyrim").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Journey").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Tecmo Bowl").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Sega Rally").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Tekken 2").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Chrono Cross").first_or_create.update price: rand(1..10) * 100
Game.where(name: "The Legend of Zelda: Twilight Princess").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Super Smash Bros. Brawl").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Dragon Quest").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Ninja Gaiden").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Herzog Zwei").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Ridge Racer").first_or_create.update price: rand(1..10) * 100
Game.where(name: "EarthBound").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Tekken 3").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Phantasy Star II").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Shenmue II").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Streets of Rage 2").first_or_create.update price: rand(1..10) * 100
Game.where(name: "The Legend of Zelda: Ocarina of Time").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Grand Theft Auto IV").first_or_create.update price: rand(1..10) * 100
Game.where(name: "SoulCalibur").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Super Mario Galaxy 2").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Super Mario Galaxy").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Metroid Prime").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Halo: Combat Evolved").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Grand Theft Auto V").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Grand Theft Auto III").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Perfect Dark").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Tony Hawk's Pro Skater 3").first_or_create.update price: rand(1..10) * 100
Game.where(name: "NFL 2K1").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Uncharted 2: Among Thieves").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Mass Effect 2").first_or_create.update price: rand(1..10) * 100
Game.where(name: "The Elder Scrolls V: Skyrim").first_or_create.update price: rand(1..10) * 100
Game.where(name: "BioShock").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Resident Evil 4").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Half-Life 2").first_or_create.update price: rand(1..10) * 100
Game.where(name: "The Legend of Zelda: The Wind Waker").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Batman: Arkham City").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Metal Gear Solid 2: Sons of Liberty").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Half-Life").first_or_create.update price: rand(1..10) * 100
Game.where(name: "GoldenEye 007").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Super Mario Galaxy").first_or_create.update price: rand(1..10) * 100
Game.where(name: "The Legend of Zelda: Ocarina of Time").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Super Mario Galaxy 2").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Grand Theft Auto V").first_or_create.update price: rand(1..10) * 100
Game.where(name: "SoulCalibur").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Uncharted 2: Among Thieves").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Super Mario 64").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Metroid Prime").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Batman: Arkham City").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Resident Evil 4").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Tekken 3").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Mass Effect 2").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Halo: Combat Evolved").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Half-Life 2").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Grand Theft Auto III").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Portal 2").first_or_create.update price: rand(1..10) * 100
Game.where(name: "The Elder Scrolls V: Skyrim").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Metal Gear Solid 2: Sons of Liberty").first_or_create.update price: rand(1..10) * 100
Game.where(name: "Grand Theft Auto: San Andreas").first_or_create.update price: rand(1..10) * 100

unless UsersGame.all.exists?
  500.times do |i|
    offset = rand(User.count)
    rand_user = User.offset(offset).first
    offset = rand(Game.count)
    rand_game = Game.offset(offset).first
    UsersGame.where(user_id: rand_user.id, game_id: rand_game.id).first_or_create
  end
end

unless CreatorsGame.all.exists?
  40.times do |i|
    offset = rand(User.count)
    rand_user = User.offset(offset).first
    offset = rand(Game.count)
    rand_game = Game.offset(offset).first
    CreatorsGame.where(user_id: rand_user.id, game_id: rand_game.id).first_or_create
  end
end
