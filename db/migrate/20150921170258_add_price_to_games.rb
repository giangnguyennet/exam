class AddPriceToGames < ActiveRecord::Migration
  def change
    add_column :games, :price, :integer
  end
end
