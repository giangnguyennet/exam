class DashboardController < ApplicationController
  def index
  end

  def exam_1
  end

  def get_prime
    ### exam_1 ####
    @result = nil
  end

  def exam_2
    @user_game = User.all.map { |u| { u.email => u.games.count } }.sort_by(&:values).last
  end

  def get_user_game
    ### exam_2                          ###
    ### u can send more global variable ###
    @result = User.new
  end

  def exam_3
  end

  def get_user_game_hard
    ### exam_3                            ###
    ### return user                       ###
    ### Can't add more global variable    ###
    ### Try to make everything in 1 query ###
    @result = User.new
  end

  def exam_4
    ### exam 4                                                            ###
    ### your have to set current_user as a user that have at least 1 game ###
    ### You can change global variable                                    ###
    #@current_user = nil ### have to change by random with at least 1 game ###
    @games = Game.all

    @current_user = User.find(UsersGame.pluck(:user_id).sample)
  end

  def exam_5
    ### exam 5                                                            ###
    ### your have to set current_user as a user that have at least 1 game ###
    ### You can change global variable                                    ###
    @current_user = nil ### have to change by random with at least 1 game ###
    @games = Game.all
  end

  def exam_6
    @games = Game.limit(5)

    @games_count_users = @games.map do |g|
      [
        g.name,
        UsersGame.where(game_id: g.id).count,
        User.find(UsersGame.where(game_id: g.id).pluck(:user_id)).map(&:email).join(', ')
      ]
    end
  end

  def exam_7
    all_game_ids = Game.pluck(:id)
    all_game_ids_someone = UsersGame.pluck(:game_id).uniq

    all_game_ids_noone = all_game_ids - all_game_ids_someone

    @all_games_noone = Game.find(all_game_ids_noone).map(&:name)
  end

  def exam_8
    @games = Game.limit(5)

    @games_count_users = @games.map do |g|
      [
        g.name,
        g.price,
        UsersGame.where(game_id: g.id).count,
        UsersGame.where(game_id: g.id).count * g.price,
      ]
    end
  end

  def exam_9
    @self_bought_games = []

    User.all.each do |u|
      bought_games = u.games
      own_games    = u.own_games

      @self_bought_games << (bought_games & own_games)
    end

    @self_bought_games.flatten!
  end

  def exam_10
    @games = Game.limit(5)

    @games_count_users = @games.map do |g|
      [
        g.name,
        UsersGame.where(game_id: g.id).count,
        User.find(UsersGame.where(game_id: g.id).pluck(:user_id)).map(&:email),
        g.creators.map(&:email)
      ]
    end
  end
end
