module ApplicationHelper

  def t_(key, options = {})
    I18n.t("general.#{key}", options)
  end

  def t_locale(key, options = {})
    I18n.t("locales.#{key}", options)
  end

  def t_title(key, options = {})
    I18n.t("titles.#{key}", options)
  end

  def t_exam(number)
    I18n.t("exam", number: number)
  end

  def t_exam_title(key, options = {})
    I18n.t("exames.#{key}.title", options)
  end

  def t_exam_content(key, options = {})
    I18n.t("exames.#{key}.content", options)
  end

  def t_boolean(boolean)
    raw '<i class="fa fa-check"></i>' if boolean
  end
end
