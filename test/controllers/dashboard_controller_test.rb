require 'test_helper'

class DashboardControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get exam_1" do
    get :exam_1
    assert_response :success
  end

  test "should get exam_2" do
    get :exam_2
    assert_response :success
  end

  test "should get exam_3" do
    get :exam_3
    assert_response :success
  end

  test "should get exam_4" do
    get :exam_4
    assert_response :success
  end

  test "should get exam_5" do
    get :exam_5
    assert_response :success
  end

  test "should get exam_6" do
    get :exam_6
    assert_response :success
  end

  test "should get exam_7" do
    get :exam_7
    assert_response :success
  end

  test "should get exam_8" do
    get :exam_8
    assert_response :success
  end

  test "should get exam_9" do
    get :exam_9
    assert_response :success
  end

  test "should get exam_10" do
    get :exam_10
    assert_response :success
  end

end
